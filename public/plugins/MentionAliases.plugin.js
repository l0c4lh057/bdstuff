//META{"name":"MentionAliases","source":"https://gitlab.com/_Lighty_/bdstuff/blob/master/public/plugins/MentionAliases.plugin.js","website":"https://_lighty_.gitlab.io/bdstuff/?plugin=MentionAliases"}*//
class MentionAliases {
  constructor() {
    this.saveSettings = this.saveSettings.bind(this);
  }
  getName() {
    return 'MentionAliases';
  }
  getVersion() {
    return '1.3.0';
  }
  getAuthor() {
    return 'Lighty';
  }
  getDescription() {
    return 'Allows you to set a custom @mention aliases for users.';
  }
  load() {}
  start() {
    let onLoaded = () => {
      try {
        if (!global.ZeresPluginLibrary) setTimeout(() => onLoaded(), 1000);
        else this.initialize();
      } catch (err) {
        ZLibrary.Logger.stacktrace(this.getName(), 'Failed to start!', err);
        ZLibrary.Logger.err(this.getName(), `If you cannot solve this yourself, contact ${this.getAuthor()} and provide the errors shown here.`);
        this.stop();
        ZLibrary.Toasts.show(`[${this.getName()}] Failed to start! Check console (CTRL + SHIFT + I, click console tab) for more error info.`, { type: 'error', timeout: 10000 });
      }
    };
    const getDir = () => {
      // from Zeres Plugin Library, copied here as ZLib may not be available at this point
      const process = require('process');
      const path = require('path');
      if (process.env.injDir) return path.resolve(process.env.injDir, 'plugins/');
      switch (process.platform) {
        case 'win32':
          return path.resolve(process.env.appdata, 'BetterDiscord/plugins/');
        case 'darwin':
          return path.resolve(process.env.HOME, 'Library/Preferences/', 'BetterDiscord/plugins/');
        default:
          return path.resolve(process.env.XDG_CONFIG_HOME ? process.env.XDG_CONFIG_HOME : process.env.HOME + '/.config', 'BetterDiscord/plugins/');
      }
    };
    this.pluginDir = getDir();
    let libraryOutdated = false;
    // I'm sick and tired of people telling me my plugin doesn't work and it's cause zlib is outdated, ffs
    if (!global.ZLibrary || !global.ZeresPluginLibrary || (bdplugins.ZeresPluginLibrary && (libraryOutdated = ZeresPluginLibrary.PluginUpdater.defaultComparator(bdplugins.ZeresPluginLibrary.plugin._config.info.version, '1.2.6')))) {
      const title = libraryOutdated ? 'Library outdated' : 'Library Missing';
      const ModalStack = BdApi.findModuleByProps('push', 'update', 'pop', 'popWithKey');
      const TextElement = BdApi.findModuleByProps('Sizes', 'Weights');
      const ConfirmationModal = BdApi.findModule(m => m.defaultProps && m.key && m.key() == 'confirm-modal');
      const confirmedDownload = () => {
        require('request').get('https://rauenzi.github.io/BDPluginLibrary/release/0PluginLibrary.plugin.js', async (error, response, body) => {
          if (error) return require('electron').shell.openExternal('https://betterdiscord.net/ghdl?url=https://raw.githubusercontent.com/rauenzi/BDPluginLibrary/master/release/0PluginLibrary.plugin.js');
          require('fs').writeFile(require('path').join(this.pluginDir, '0PluginLibrary.plugin.js'), body, () => {
            setTimeout(() => {
              if (!global.bdplugins.ZeresPluginLibrary) return BdApi.alert('Notice', `Due to you using EnhancedDiscord instead of BetterDiscord, you'll have to reload your Discord before ${this.getName()} starts working. Just press CTRL + R to reload and ${this.getName()} will begin to work!`);
              onLoaded();
            }, 1000);
          });
        });
      };
      if (!ModalStack || !ConfirmationModal || !TextElement) {
        BdApi.alert('Uh oh', `Looks like you${libraryOutdated ? 'r Zeres Plugin Library was outdated!' : ' were missing Zeres Plugin Library!'} Also, failed to show a modal, so it has been ${libraryOutdated ? 'updated' : 'downloaded and loaded'} automatically.`);
        confirmedDownload();
        return;
      }
      ModalStack.push(props => {
        return BdApi.React.createElement(
          ConfirmationModal,
          Object.assign(
            {
              header: title,
              children: [TextElement({ color: TextElement.Colors.PRIMARY, children: [`The library plugin needed for ${this.getName()} is ${libraryOutdated ? 'outdated' : 'missing'}. Please click Download Now to ${libraryOutdated ? 'update' : 'install'} it.`] })],
              red: false,
              confirmText: 'Download Now',
              cancelText: 'Cancel',
              onConfirm: () => confirmedDownload()
            },
            props
          )
        );
      });
    } else onLoaded();
  }
  stop() {
    try {
      this.shutdown();
    } catch (err) {
      ZeresPluginLibrary.Logger.stacktrace(this.getName(), 'Failed to stop!', err);
    }
  }
  getChanges() {
    return [
      /* {
        title: 'added',
        type: 'added',
        items: ['Added option to show alias tag in DMs list.', 'Added option to show alias after AKA in DMs.']
      }, */
      {
        title: 'fixes',
        type: 'fixed',
        items: ['Fixed plugin not working due to context menu update.']
      } /* ,
      {
        title: 'Improved',
        type: 'improved',
        items: ['Defined aliases menu now shows profile pictures.']
      } */ /* ,

                {
                    title: 'Gimme some time',
                    type: 'progress',
                    items: ['Expand Stats modal to show per server and per channel stats, also show disk space usage.']
                } */
    ];
  }
  initialize() {
    ZeresPluginLibrary.PluginUpdater.checkForUpdate(this.getName(), this.getVersion(), `https://_lighty_.gitlab.io/bdstuff/plugins/${this.getName()}.plugin.js`);
    let defaultSettings = {
      displayUpdateNotes: true,
      displayRightCompact: false,
      displayMessageTags: true,
      displayMemberTags: true,
      displayPopupTags: true,
      displayOwnerTags: true,
      displayAKATags: true,
      displayDMTags: true,
      displayButton: true,
      aliases: {},
      groups: [],
      versionInfo: ''
    };
    this.settings = this.loadData(this.getName(), defaultSettings);
    if (!this.settings || !Object.keys(this.settings).length) this.settings = defaultSettings;
    if (!Array.isArray(this.settings.groups)) this.settings.groups = [];
    if (typeof this.settings.aliases !== 'object') this.settings.Object = {};

    if (this.settings.versionInfo !== this.getVersion() && this.settings.displayUpdateNotes) {
      ZeresPluginLibrary.Modals.showChangelogModal(this.getName() + ' Changelog', this.getVersion(), this.getChanges());
      this.settings.versionInfo = this.getVersion();
      this.saveSettings();
    }

    this.selectors = {
      chatButtons: '.' + ZLibrary.WebpackModules.getByProps('buttons', 'channelTextArea').buttons.split(' ')[0]
    };
    this.openAliasesButton = ZLibrary.DOMTools.parseHTML(
      `<button aria-label="Open mention alises" tabindex="2" type="button" class="${ZLibrary.WebpackModules.getByProps('active', 'buttonWrapper').buttonWrapper} ${ZLibrary.WebpackModules.getByProps('button', 'disabledButtonWrapper').button} ${ZLibrary.WebpackModules.getByProps('lookBlank').lookBlank} ${ZLibrary.WebpackModules.getByProps('colorBrand').colorBrand} ${ZLibrary.WebpackModules.getByProps('grow').grow}"><div class="${ZLibrary.WebpackModules.getByProps('contents').contents} ${ZLibrary.WebpackModules.getByProps('button', 'pulseButton').button} ${ZLibrary.WebpackModules.getByProps('button', 'channelTextArea').button}"><svg x="0" y="0" name="Nova_At" class="${
        ZLibrary.WebpackModules.getByProps('pulseButton', 'icon').icon
      }" aria-hidden="false" width="24" height="24" viewBox="0 0 24 24"><path fill="currentColor" d="M12 2C6.486 2 2 6.486 2 12C2 17.515 6.486 22 12 22C14.039 22 15.993 21.398 17.652 20.259L16.521 18.611C15.195 19.519 13.633 20 12 20C7.589 20 4 16.411 4 12C4 7.589 7.589 4 12 4C16.411 4 20 7.589 20 12V12.782C20 14.17 19.402 15 18.4 15L18.398 15.018C18.338 15.005 18.273 15 18.209 15H18C17.437 15 16.6 14.182 16.6 13.631V12C16.6 9.464 14.537 7.4 12 7.4C9.463 7.4 7.4 9.463 7.4 12C7.4 14.537 9.463 16.6 12 16.6C13.234 16.6 14.35 16.106 15.177 15.313C15.826 16.269 16.93 17 18 17L18.002 16.981C18.064 16.994 18.129 17 18.195 17H18.4C20.552 17 22 15.306 22 12.782V12C22 6.486 17.514 2 12 2ZM12 14.599C10.566 14.599 9.4 13.433 9.4 11.999C9.4 10.565 10.566 9.399 12 9.399C13.434 9.399 14.6 10.565 14.6 11.999C14.6 13.433 13.434 14.599 12 14.599Z"></path></svg></div></button>`
    );
    this.openAliasesButton.addEventListener('click', () => this.showAliasesPopup(this.openAliasesButton));
    this.observer.chatClass = ZLibrary.WebpackModules.getByProps('chat').chat.split(/ /g)[0];
    this.observer.chatContentClass = ZLibrary.WebpackModules.getByProps('chatContent').chatContent.split(/ /g)[0];
    this.showAliasesPopup.multiClasses = {
      popup: ZLibrary.WebpackModules.getByProps('header', 'messagesPopoutWrap').messagesPopoutWrap + ' ' + ZLibrary.WebpackModules.getByProps('themedPopout').themedPopout
    };
    this.onChannelSwitch();

    this.createModal.confirmationModal = ZeresPluginLibrary.DiscordModules.ConfirmationModal;

    this.patchedModules = [];

    this.unpatches = [];
    this.unpatches.push(ZLibrary.Patcher.after(this.getName(), ZLibrary.WebpackModules.getByProps('queryChannelUsers'), 'queryMentionResults', (_, args, ret) => this.queryChannelUsers(ret, args[0])));
    this.unpatches.push(
      ZLibrary.Patcher.instead(this.getName(), ZLibrary.WebpackModules.getByDisplayName('Autocomplete').Role.prototype, 'renderContent', (thisObj, _, orig) => {
        var role = thisObj.props.role;
        if (!role.MA) return orig();
        return this.renderAlias(role.name, role.note, role.colorString);
      })
    );
    this.unpatches.push(
      ZLibrary.Patcher.instead(this.getName(), ZLibrary.WebpackModules.getByPrototypes('selectAutocompletion').prototype, 'selectAutocompletion', (thisObj, args, orig) => {
        const selected = args[0];
        const type = thisObj.state.autocompleteType;
        if (type !== 'MENTIONS') return orig(...args);
        const autocompletes = thisObj.props.autocompletes;
        const role = autocompletes.roles[selected - autocompletes.users.length - autocompletes.globals.length];
        if (!role || !role.MA) return orig(...args);
        thisObj.insertText(role.mentioned_users, 0);
      })
    );
    this.ContextMenuItem = ZeresPluginLibrary.DiscordModules.ContextMenuItem;
    this.ContextMenuActions = ZeresPluginLibrary.DiscordModules.ContextMenuActions;
    this.ContextMenuGroup = ZeresPluginLibrary.DiscordModules.ContextMenuItemsGroup;
    this.SubMenuItem = ZeresPluginLibrary.WebpackModules.find(m => m.default && m.default.displayName && m.default.displayName.includes('SubMenuItem')).default;

    this.createAlias.messageCozyTag = `mentionAlias ${ZLibrary.WebpackModules.getByProps('botTagRegular').botTagRegular} ${ZLibrary.WebpackModules.getByProps('botTagCozy').botTagCozy}`;
    this.createAlias.messageCompactTag = `mentionAlias ${ZLibrary.WebpackModules.getByProps('botTagRegular').botTagRegular} ${ZLibrary.WebpackModules.getByProps('botTagCompact').botTagCompact}`;
    this.createAlias.memberTag = `mentionAlias ${ZLibrary.WebpackModules.getByProps('botTagRegular').botTagRegular} ${ZLibrary.WebpackModules.getByProps('botTag', 'member').botTag}`;
    this.createAlias.popoutTag = `mentionAlias ${ZLibrary.WebpackModules.getByProps('botTagRegular').botTagRegular} ${ZLibrary.WebpackModules.getByProps('bot', 'nameTag').bot}`;

    this.unpatches.push(
      ZeresPluginLibrary.Patcher.before(this.getName(), ZLibrary.DiscordModules.ContextMenuActions, 'openContextMenu', (_this, args, ret) => {
        const old = args[1];
        args[1] = e => {
          const ret2 = old(e);
          if (typeof ret2.type !== 'function') return ret2;
          const old2 = ret2.type;
          const onContext = this.handleContextMenu.bind(this);
          ret2.type = function(e) {
            const ret3 = new old2(e);
            if (ret3.render) {
              const old3 = ret3.render.bind(ret3);
              ret3.render = () => {
                const ret4 = old3();
                onContext({ props: e }, null, ret4);
                return ret4;
              };
            } else {
              onContext({ props: e }, null, ret3);
            }
            return ret3;
          };
          return ret2;
        };
      })
    );

    const MessageModule = ZLibrary.WebpackModules.getByProps('MessageUsername', 'Message');
    this.unpatches.push(
      ZeresPluginLibrary.Patcher.after(this.getName(), MessageModule.MessageUsername.prototype, 'render', (thisObj, args, returnValue) => {
        if (!thisObj.props.message.author || !this.settings.displayMessageTags) return;
        const alias = this.getUserAlias(thisObj.props.message.author.id);
        if (!alias) return;
        const oChildren = returnValue.props.children;
        returnValue.props.children = e => {
          const ret2 = oChildren(e);
          if (this.isCompact() && !this.settings.displayRightCompact) ret2.props.children.unshift(this.createAlias(alias, this.createAlias.messageCompactTag));
          else ret2.props.children.push(this.createAlias(alias, this.createAlias.messageCozyTag));
          return ret2;
        };
      })
    );
    this.patchedModules.push(new ZLibrary.ReactComponents.ReactComponent('Message', MessageModule.Message, `.${ZeresPluginLibrary.WebpackModules.getByProps('containerCozyBounded').containerCozyBounded.split(/ /g)[0]} > div:not(.${ZeresPluginLibrary.WebpackModules.getByProps('content', 'marginCompactIndent').content.split(/ /g)[0]})`));
    const MemberListItem = ZLibrary.WebpackModules.getByDisplayName('MemberListItem');
    this.unpatches.push(
      ZeresPluginLibrary.Patcher.after(this.getName(), MemberListItem.prototype, 'render', (thisObj, args, returnValue) => {
        if (!thisObj.props.user || !this.settings.displayMemberTags) return;
        const alias = this.getUserAlias(thisObj.props.user.id);
        if (!alias) return;
        returnValue.props.decorators.props.children.push(this.createAlias(alias, this.createAlias.memberTag));
      })
    );
    this.patchedModules.push(new ZLibrary.ReactComponents.ReactComponent('MemberListItem', MemberListItem, new ZLibrary.DOMTools.Selector(ZLibrary.WebpackModules.getByProps('member', 'offline').member).toString()));
    const PrivateChannel = ZLibrary.WebpackModules.getByDisplayName('PrivateChannel');
    this.unpatches.push(
      ZeresPluginLibrary.Patcher.after(this.getName(), PrivateChannel.prototype, 'render', (thisObj, args, returnValue) => {
        if (!thisObj.props.user || !this.settings.displayDMTags) return;
        const alias = this.getUserAlias(thisObj.props.user.id);
        if (!alias) return;
        const orig = returnValue.type;
        returnValue.type = e => {
          const ret = orig(e);
          const nameAndDecorators = ZLibrary.Utilities.getNestedProp(ret, 'props.children.props.children.1.props.children.0.props.children');
          if (!nameAndDecorators) return ret;
          nameAndDecorators.push(this.createAlias(alias, this.createAlias.memberTag));
          return ret;
        };
      })
    );
    this.patchedModules.push(new ZLibrary.ReactComponents.ReactComponent('PrivateChannel', PrivateChannel, new ZLibrary.DOMTools.Selector(ZLibrary.WebpackModules.getByProps('channel', 'closeButton').channel).toString()));
    this.unpatches.push(
      ZeresPluginLibrary.Patcher.after(this.getName(), ZLibrary.WebpackModules.getByProps('getNicknames'), 'getNicknames', (thisObj, args, returnValue) => {
        if (!this.settings.displayAKATags) return;
        const userId = args[0];
        const alias = this.getUserAlias(userId);
        if (!alias) return;
        returnValue.push(alias);
      })
    );
    ZeresPluginLibrary.PluginUtilities.addStyle(
      'MA-CSS',
      `
                .mentionAlias {
                    background: #56555e;
                    color: white;
                }
                `
    );

    this.createButton.classes = {
      button: (function() {
        let buttonData = ZeresPluginLibrary.WebpackModules.getByProps('button', 'colorBrand');
        return `${buttonData.button} ${buttonData.lookFilled} ${buttonData.colorBrand} ${buttonData.sizeSmall} ${buttonData.grow}`;
      })(),
      buttonContents: ZeresPluginLibrary.WebpackModules.getByProps('button', 'colorBrand').contents
    };
    this.promises = {
      state: { cancelled: false },
      cancel() {
        this.state.cancelled = true;
      }
    };
    ZLibrary.Utilities.suppressErrors(this.patchUserPopouts.bind(this), 'user popout patch')(this.promises.state);
    ZLibrary.Utilities.suppressErrors(this.patchUserModals.bind(this), 'user modal patch')(this.promises.state);

    this.forceUpdateAll();
  }
  shutdown() {
    const tryUnpatch = fn => {
      try {
        // things can bug out, best to reload tbh, should maybe warn the user?
        fn();
      } catch (e) {
        ZeresPluginLibrary.Logger.stacktrace(this.getName(), 'Error unpatching', e);
      }
    };
    if (this.unpatches) for (let unpatch of this.unpatches) tryUnpatch(unpatch);
    if (this.openAliasesButton) this.openAliasesButton.remove();
    ZeresPluginLibrary.PluginUtilities.removeStyle('MA-CSS');
    if (this.promises) this.promises.cancel();
    if (this.patchedModules) this.forceUpdateAll();
  }
  observer({ addedNodes }) {
    for (const change of addedNodes) {
      if ((typeof change.className === 'string' && change.className.indexOf(this.observer.chatClass) !== -1) || (typeof change.className === 'string' && change.className.indexOf(this.observer.chatContentClass) !== -1)) {
        this.onChannelSwitch();
      }
    }
  }
  loadData(name, construct = {}) {
    try {
      return Object.assign(construct, BdApi.getData(name, 'settings')); // zeres library turns all arrays into objects.. for.. some reason?
    } catch (e) {
      return {}; // error handling not done here
    }
  }
  buildSetting(data) {
    // compied from ZLib actually
    const { name, note, type, value, onChange, id } = data;
    let setting = null;
    if (type == 'color') {
      setting = new ZeresPluginLibrary.Settings.ColorPicker(name, note, value, onChange, { disabled: data.disabled }); // DOESN'T WORK, REEEEEEEEEE
    } else if (type == 'dropdown') {
      setting = new ZeresPluginLibrary.Settings.Dropdown(name, note, value, data.options, onChange);
    } else if (type == 'file') {
      setting = new ZeresPluginLibrary.Settings.FilePicker(name, note, onChange);
    } else if (type == 'keybind') {
      setting = new ZeresPluginLibrary.Settings.Keybind(name, note, value, onChange);
    } else if (type == 'radio') {
      setting = new ZeresPluginLibrary.Settings.RadioGroup(name, note, value, data.options, onChange, { disabled: data.disabled });
    } else if (type == 'slider') {
      const options = {};
      if (typeof data.markers != 'undefined') options.markers = data.markers;
      if (typeof data.stickToMarkers != 'undefined') options.stickToMarkers = data.stickToMarkers;
      setting = new ZeresPluginLibrary.Settings.Slider(name, note, data.min, data.max, value, onChange, options);
    } else if (type == 'switch') {
      setting = new ZeresPluginLibrary.Settings.Switch(name, note, value, onChange, { disabled: data.disabled });
    } else if (type == 'textbox') {
      setting = new ZeresPluginLibrary.Settings.Textbox(name, note, value, onChange, { placeholder: data.placeholder || '' });
    }
    return setting;
  }
  createSetting(data) {
    const current = Object.assign({}, data);
    if (!current.onChange) {
      current.onChange = value => {
        this.settings[current.id] = value;
        if (current.callback) current.callback(value);
      };
    }
    if (typeof current.value === 'undefined') current.value = this.settings[current.id];
    return this.buildSetting(current);
  }
  createGroup(group) {
    const { name, id, collapsible, shown, settings } = group;

    const list = [];
    for (let s = 0; s < settings.length; s++) list.push(this.createSetting(settings[s]));

    const settingGroup = new ZeresPluginLibrary.Settings.SettingGroup(name, { shown, collapsible }).append(...list);
    settingGroup.group.id = id; // should generate the id in here instead?
    return settingGroup;
  }
  getSettingsPanel() {
    // todo, sort out the menu
    const list = [];
    list.push(
      this.createGroup({
        name: 'Keybinds',
        collapsible: false,
        shown: true,
        settings: [
          {
            name: 'Display tags menu button',
            id: 'displayButton',
            type: 'switch',
            callback: val => {
              if (!val) this.openAliasesButton.remove();
              else this.onChannelSwitch();
            }
          },
          {
            name: 'Display owner tags',
            id: 'displayOwnerTags',
            type: 'switch',
            callback: () => this.forceUpdateAll()
          },
          {
            name: 'Display tag in user popups',
            id: 'displayPopupTags',
            type: 'switch',
            callback: () => this.forceUpdateAll()
          },
          {
            name: 'Display tags in members list',
            id: 'displayMemberTags',
            type: 'switch',
            callback: () => this.forceUpdateAll()
          },
          {
            name: 'Display tag in messages',
            id: 'displayMessageTags',
            type: 'switch',
            callback: () => this.forceUpdateAll()
          },
          {
            name: 'Display tag on the right side of the name in compact mode',
            id: 'displayRightCompact',
            type: 'switch',
            callback: () => this.forceUpdateAll()
          },
          {
            name: 'Display alias in AKA in DMs',
            id: 'displayAKATags',
            type: 'switch',
            callback: () => this.forceUpdateAll()
          },
          {
            name: 'Display tag in DMs list',
            id: 'displayDMTags',
            type: 'switch',
            callback: () => this.forceUpdateAll()
          },
          {
            name: 'Display update notes',
            id: 'displayUpdateNotes',
            type: 'switch'
          }
        ]
      })
    );

    const div = document.createElement('div');
    div.style.display = 'inline-flex';
    div.appendChild(this.createButton('Changelog', () => ZeresPluginLibrary.Modals.showChangelogModal(this.getName() + ' Changelog', this.getVersion(), this.getChanges())));
    div.appendChild(this.createButton('Donate', () => this.nodeModules.electron.shell.openExternal('https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DZUL9UZDDRLB8&source=url')));
    let button = div.firstElementChild;
    while (button) {
      button.style.marginRight = button.style.marginLeft = `5px`;
      button = button.nextElementSibling;
    }

    list.push(div);

    return ZeresPluginLibrary.Settings.SettingPanel.build(_ => this.saveSettings(), ...list);
  }
  saveSettings() {
    ZeresPluginLibrary.PluginUtilities.saveSettings(this.getName(), this.settings);
  }
  createButton(label, callback) {
    const classes = this.createButton.classes;
    const ret = ZLibrary.DOMTools.parseHTML(`<button type="button" class="${classes.button}"><div class="${classes.buttonContents}">${label}</div></button>`);
    if (callback) ret.addEventListener('click', callback);
    return ret;
  }
  onChannelSwitch() {
    this.selectedGuild = ZLibrary.DiscordModules.GuildStore.getGuild(ZLibrary.DiscordModules.SelectedGuildStore.getGuildId());
    if (!this.selectedGuild) return;
    const buttons = document.querySelector(this.selectors.chatButtons);
    if (!buttons || !buttons.childElementCount) return;
    buttons.prepend(this.openAliasesButton);
    this.selectedChannelUsers = ZLibrary.DiscordModules.GuildMemberStore.getMemberIds(this.selectedGuild.id);
    this.textArea = ZLibrary.ReactTools.getOwnerInstance(document.querySelector('.' + ZLibrary.WebpackModules.getByProps('channelTextArea').channelTextArea.split(' ')[0]));
  }
  isCompact() {
    return ZeresPluginLibrary.DiscordAPI.UserSettings.displayCompact; // can't get a reference
  }
  patchTag(tag, alias) {
    const orig = tag.type;
    tag.type = e => {
      const ret = orig(e);
      const orig2 = ret.type;
      ret.type = e => {
        const ret2 = orig2(e);
        ret2.props.children.push(this.createAlias(alias, this.createAlias.popoutTag));
        return ret2;
      };
      return ret;
    };
  }
  /* copied from BetterRoleColors by Zerebos, he did it better /shrug */
  async patchUserPopouts(promiseState) {
    const UserPopout = await ZLibrary.ReactComponents.getComponentByName('UserPopout', ZLibrary.DiscordSelectors.UserPopout.userPopout);
    if (promiseState.cancelled) return;
    this.unpatches.push(
      ZLibrary.Patcher.after(this.getName(), UserPopout.component.prototype, 'render', (thisObject, _, returnValue) => {
        const alias = this.getUserAlias(thisObject.props.userId);
        const tag = ZLibrary.Utilities.getNestedProp(returnValue, 'props.children.props.children.0.props.children.0.props.children.1.props.children.1.props.children');
        if (!this.settings.displayPopupTags || !alias || !tag) return;
        this.patchTag(tag, alias);
      })
    );
    this.patchedModules.push(UserPopout);
    UserPopout.forceUpdateAll();
  }
  async patchUserModals(promiseState) {
    const UserProfileBody = await ZLibrary.ReactComponents.getComponentByName('UserProfileBody', ZLibrary.DiscordSelectors.UserModal.root);
    if (promiseState.cancelled) return;
    this.unpatches.push(
      ZLibrary.Patcher.after(this.getName(), UserProfileBody.component.prototype, 'render', (thisObject, _, returnValue) => {
        const alias = this.getUserAlias(thisObject.props.user.id);
        const tag = ZLibrary.Utilities.getNestedProp(returnValue, 'props.children.props.children.0.props.children.0.props.children.1.props.children.0');
        if (!this.settings.displayPopupTags || !alias || !tag) return;
        this.patchTag(tag, alias);
      })
    );
    this.patchedModules.push(UserProfileBody);
    UserProfileBody.forceUpdateAll();
  }
  forceUpdateAll() {
    for (const module of this.patchedModules) module.forceUpdateAll();
  }
  createAlias(alias, className) {
    return ZLibrary.DiscordModules.React.createElement('span', { className: className }, alias);
  }
  getUserAlias(id) {
    const alias = this.settings.aliases[id];
    if (alias) return alias;
    if (this.settings.displayOwnerTags && ZLibrary.DiscordAPI.currentGuild && ZLibrary.DiscordAPI.currentGuild.ownerId === id) return 'Owner';
  }
  createModal(options, name) {
    const modal = this.createModal.confirmationModal;
    ZeresPluginLibrary.DiscordModules.ModalStack.push(function(props) {
      return ZeresPluginLibrary.DiscordModules.React.createElement(modal, Object.assign(options, props));
    });
  }
  queryChannelUsers(ret, query) {
    if (!query.length) return;
    const mentions = this.queryAliases(query);
    for (const userId in mentions.users) {
      const user = ZLibrary.DiscordModules.UserStore.getUser(userId);
      if (!user) continue;
      let idx = 0;
      if ((idx = ret.users.findIndex(m => m.user.id === userId)) !== -1) {
        ret.users[idx].nick = mentions.users[userId];
        continue;
      }
      ret.users.unshift({
        /* comparator: mentions.users[userId].toLowerCase(), */
        nick: mentions.users[userId],
        score: mentions.users[userId] === 'Owner' ? 10 : 10, // I don't think this does anything
        user: user,
        status: ZLibrary.WebpackModules.getByProps('getStatus').getStatus(userId)
      });
    }
    for (const group of mentions.groups) {
      const groupUsers = this.getGroupUsers(group.users);
      ret.roles.unshift({
        name: group.name,
        note: groupUsers.note,
        mentioned_users: groupUsers.tags,
        MA: true
      });
    }
    return ret;
  }
  getGroupUsers(users) {
    const ret = {
      note: '',
      tags: ''
    };
    for (const userId of users) {
      const user = ZLibrary.DiscordModules.UserStore.getUser(userId);
      if (!user) continue;
      if (ret.note.length) ret.note += ' ';
      if (ret.tags.length) ret.tags += ' ';
      ret.note += user.username;
      ret.tags += `@${user.username}#${user.discriminator}`;
    }
    return ret;
  }
  queryAliases(query) {
    const ret = {
      users: {},
      groups: []
    };
    for (const userId in this.settings.aliases) {
      if (this.selectedChannelUsers.findIndex(m => m === userId) === -1) continue;
      if (this.settings.aliases[userId].toLowerCase().includes(query.toLowerCase())) ret.users[userId] = this.settings.aliases[userId];
    }
    if (this.selectedGuild && 'owner'.includes(query.toLowerCase())) ret.users[this.selectedGuild.ownerId] = 'Owner';
    for (const group of this.settings.groups) {
      if (!group.name.toLowerCase().includes(query.toLowerCase())) continue;
      const users = [];
      for (const userId of group.users) {
        if (this.selectedChannelUsers.findIndex(m => m === userId) === -1) continue;
        users.push(userId);
      }
      if (!users.length) continue;
      ret.groups.push({
        name: group.name,
        users: users
      });
    }
    return ret;
  }
  renderHeader(name, paddedTop) {
    const text = ZLibrary.WebpackModules.getByDisplayName('Text');
    return ZeresPluginLibrary.DiscordModules.React.createElement(
      text,
      {
        className: ZLibrary.WebpackModules.getByProps('autocomplete', 'contentTitle').contentTitle,
        weight: text.Weights.SEMIBOLD,
        size: text.Sizes.SMALL,
        style: {
          paddingBottom: 8,
          paddingTop: paddedTop ? 8 : 0
        }
      },
      name
    );
  }
  renderAlias(name, description, color, noAt) {
    return ZeresPluginLibrary.DiscordModules.React.createElement(
      ZLibrary.WebpackModules.getByDisplayName('Flex'),
      {
        align: ZLibrary.WebpackModules.getByDisplayName('Flex').Align.CENTER,
        className: ZLibrary.WebpackModules.getByProps('autocomplete', 'content').content
      },
      ZeresPluginLibrary.DiscordModules.React.createElement(
        ZLibrary.WebpackModules.getByDisplayName('Flex').Child,
        {
          grow: 1
        },
        ZeresPluginLibrary.DiscordModules.React.createElement(
          ZLibrary.WebpackModules.getByDisplayName('Text'),
          {
            style: {
              color: color
            }
          },
          noAt ? undefined : '@',
          name
        )
      ),
      ZeresPluginLibrary.DiscordModules.React.createElement(
        ZLibrary.WebpackModules.getByDisplayName('Text'),
        {
          className: ZLibrary.WebpackModules.getByProps('autocomplete', 'description').description
        },
        description
      )
    );
  }
  showAliasesPopup(target) {
    const popupClassName = this.showAliasesPopup.multiClasses.popup;
    const aliases = [];
    // selectorSelected-1_M1WV selector-2IcQBU da-selectorSelected da-selector selectable-3dP3y- da-selectable
    const renderAlias = (name, description, tags, identifier, isUser, user) => {
      const ret = ZeresPluginLibrary.DiscordModules.React.createElement(
        'div',
        {
          className: ZLibrary.WebpackModules.getByProps('autocomplete', 'selector').selector + ' ' + ZLibrary.WebpackModules.getByProps('autocomplete', 'selector').selectable + (isUser ? ' layout-2DM8Md da-layout' : ''),
          onMouseEnter: e => {
            //hack but ok
            let target = e.target;
            while (target && !target.hasClass(ZLibrary.WebpackModules.getByProps('autocomplete', 'selector').selector)) target = target.parentElement;
            target.addClass(ZLibrary.WebpackModules.getByProps('autocomplete', 'selector').selectorSelected);
          },
          onMouseLeave: e => {
            let target = e.target;
            while (target && !target.hasClass(ZLibrary.WebpackModules.getByProps('autocomplete', 'selector').selector)) target = target.parentElement;
            target.removeClass(ZLibrary.WebpackModules.getByProps('autocomplete', 'selector').selectorSelected);
            target.addClass(ZLibrary.WebpackModules.getByProps('autocomplete', 'selector').selector);
          },
          onClick: () => {
            this.textArea.insertText(tags + ' ', 0);
          },
          onContextMenu: e => {
            if (name === 'Owner') return;
            const menu = new ZeresPluginLibrary.ContextMenu.Menu();
            const remove = new ZeresPluginLibrary.ContextMenu.TextItem('Remove');
            const edit = new ZeresPluginLibrary.ContextMenu.TextItem('Edit');
            if (isUser) {
              remove.element.addEventListener('click', () => {
                menu.removeMenu();
                delete this.settings.aliases[identifier];
                this.saveSettings();
                this.forceUpdateAll();
              });
              edit.element.addEventListener('click', () => {
                menu.removeMenu();
                const user = ZLibrary.DiscordModules.UserStore.getUser(identifier);
                if (!user) return;
                this.saveAliasModal(identifier, user.username);
              });
            } else {
              const groupIDX = this.settings.groups.findIndex(m => m.name === identifier);
              remove.element.addEventListener('click', () => {
                menu.removeMenu();
                this.settings.groups.splice(groupIDX, 1);
                this.saveSettings();
              });
              edit.element.addEventListener('click', () => {
                menu.removeMenu();
                const duplicatedUsers = this.settings.groups[groupIDX].users.slice(0);
                const duplicatedName = this.settings.groups[groupIDX].name.slice(0);
                ZeresPluginLibrary.DiscordModules.ModalStack.push(_ => {
                  return ZeresPluginLibrary.DiscordModules.React.createElement(EditGroupModal, {
                    header: 'Edit group',
                    className: '',
                    users: duplicatedUsers,
                    value: duplicatedName,
                    onConfirm: (users, name) => {
                      this.settings.groups[groupIDX].users = users;
                      if (name.length) this.settings.groups[groupIDX].name = name;
                      if (!users.length) this.settings.groups.splice(groupIDX, 1);
                      this.saveSettings();
                    }
                  });
                });
              });
            }
            menu.addItems(remove, edit);
            menu.element.style.position = 'absolute';
            menu.element.style.zIndex = '99999';
            for (let ch of menu.element.children) ch.addClass('clickable-11uBi- da-clickable'); // HACK
            menu.show(e.clientX, e.clientY);
          }
        },
        isUser
          ? ZLibrary.DiscordModules.React.createElement(
              'div',
              {
                className: 'avatar-3uk_u9 da-avatar'
              },
              EditGroupModal.prototype.renderAvatar(user)
            )
          : undefined,
        this.renderAlias(name, description, '', isUser)
      );
      return ret;
    };
    const mentions = this.queryAliases('');
    if (Object.keys(mentions.users).length) {
      aliases.push(this.renderHeader('Members'));
      for (const userId in mentions.users) {
        const user = ZLibrary.DiscordModules.UserStore.getUser(userId);
        if (!user) continue;
        aliases.push(renderAlias(mentions.users[userId], user.username + '#' + user.discriminator, '@' + user.username + '#' + user.discriminator, userId, true, user));
      }
      if (aliases.length === 1) aliases.splice(0, 1);
    }
    if (mentions.groups.length) {
      aliases.push(this.renderHeader('Groups', true));
      let addedGroups = 0;
      for (const group of mentions.groups) {
        const groupUsers = this.getGroupUsers(group.users);
        if (!groupUsers.note.length) continue;
        if (groupUsers.note.length > 32) groupUsers.note = groupUsers.note.substr(0, 32 - 3) + '...';
        aliases.push(renderAlias(group.name, groupUsers.note, groupUsers.tags, group.name));
        addedGroups++;
      }
      if (!addedGroups) aliases.splice(0, 1);
    }
    if (!aliases.length) {
      aliases.push(
        ZLibrary.DiscordModules.React.createElement(
          'div',
          {
            className: ZLibrary.WebpackModules.getByProps('emptyPlaceholder').emptyPlaceholder
          },
          ZLibrary.DiscordModules.React.createElement(
            'div',
            {
              className: ZLibrary.WebpackModules.getByProps('emptyPlaceholder').body
            },
            'Well this is awkward.. No aliases\nfound, not even the owner!'
          )
        )
      );
    }
    ZLibrary.DiscordModules.PopoutOpener.openPopout(
      target,
      {
        preventCloseFromModal: true,
        showArrow: true,
        position: 'top',
        render: props => {
          return ZLibrary.DiscordModules.React.createElement(
            'div',
            {
              className: popupClassName,
              style: {
                maxHeight: ZLibrary.Structs.Screen.height - 43 - 25 - 40 /* ,
                                    width: 840 */
              }
            },
            ZLibrary.WebpackModules.getByProps('Header', 'EmptyStateBottom').Header({
              title: 'Defined User Aliases'
            }),
            ZLibrary.DiscordModules.React.createElement(
              ZLibrary.WebpackModules.getByDisplayName('VerticalScroller'),
              {
                className: ZLibrary.WebpackModules.getByProps('header', 'messagesPopoutWrap').messagesPopout
              },
              ...aliases
            ),
            false
          );
        }
      },
      'MentionAliases'
    );
  }
  saveAliasModal(userId, username) {
    let ref;
    let val = this.settings.aliases[userId] || '';
    const saveAlias = () => {
      if (val.length) this.settings.aliases[userId] = val;
      else delete this.settings.aliases[userId];
      this.saveSettings();
      this.forceUpdateAll();
    };
    this.createModal({
      confirmText: 'Save',
      cancelText: 'Cancel',
      header: 'Set Alias',
      size: ZeresPluginLibrary.Modals.ModalSizes.SMALL,
      red: false,
      children: [
        ZLibrary.DiscordModules.React.createElement(
          ZLibrary.WebpackModules.getByDisplayName('FormItem'),
          {
            title: 'Alias',
            className: ZLibrary.WebpackModules.getByProps('input', 'card').input
          },
          ZLibrary.DiscordModules.React.createElement(ZLibrary.WebpackModules.getByDisplayName('TextInput'), {
            ref: e => {
              ref = e;
            },
            value: val,
            placeholder: username,
            onChange: value => {
              ref.props.value = value;
              ref.forceUpdate();
              val = value;
            },
            autoFocus: 1,
            maxLength: 16
          })
        ),
        ZLibrary.DiscordModules.React.createElement(
          ZLibrary.WebpackModules.find(m => m.Link && m.Link.displayName === 'ButtonLink'),
          {
            look: ZLibrary.WebpackModules.find(m => m.Link && m.Link.displayName === 'ButtonLink').Looks.LINK,
            size: ZLibrary.WebpackModules.find(m => m.Link && m.Link.displayName === 'ButtonLink').Sizes.NONE,
            onClick: () => {
              ref.props.value = val = '';
              ref.forceUpdate();
            },
            color: ZLibrary.WebpackModules.getByProps('primary').primary,
            className: ZLibrary.WebpackModules.getByProps('reset', 'card').reset,
            style: {
              marginBottom: 0
            }
          },
          'Reset Alias'
        )
      ],
      onConfirm: () => saveAlias()
    });
  }
  handleContextMenu(thisObj, args, ret) {
    if (!thisObj.props.user) return;
    const userId = thisObj.props.user.id;
    const mainItems = [];
    const createItem = (label, callback) => {
      return ZeresPluginLibrary.DiscordModules.React.createElement(this.ContextMenuItem, {
        label: label,
        action: () => {
          this.ContextMenuActions.closeContextMenu();
          if (callback) callback();
        }
      });
    };
    const createSubMenu = (label, items) => {
      return ZeresPluginLibrary.DiscordModules.React.createElement(this.SubMenuItem, { label: label, render: items, invertChildY: true });
    };

    const groupSubmenuItems = [];

    const groupSubmenu = groupIDX => {
      const group = this.settings.groups[groupIDX];
      const addedIndex = group.users.findIndex(m => m === userId);
      const added = addedIndex !== -1;
      groupSubmenuItems.push(
        createSubMenu(group.name, [
          createItem(added ? 'Remove' : 'Add', () => {
            if (added) {
              group.users.splice(
                group.users.findIndex(m => m === userId),
                1
              );
              if (!group.users.length) this.settings.groups.splice(groupIDX, 1);
            } else group.users.push(userId);
            this.saveSettings();
            ZLibrary.Toasts.success(added ? 'Removed!' : 'Added!');
          }),
          createItem('Remove Group', () => {
            this.settings.groups.splice(groupIDX, 1);
            this.saveSettings();
            ZLibrary.Toasts.success('Removed!');
          })
        ])
      );
    };

    for (let i = 0; i < this.settings.groups.length; i++) groupSubmenu(i);

    groupSubmenuItems.push(
      createItem('New Group', () => {
        let ref;
        const saveGroup = () => {
          this.settings.groups.push({
            name: ref.props.value,
            users: [userId]
          });
          this.saveSettings();
        };
        this.createModal({
          confirmText: 'Save',
          cancelText: 'Cancel',
          header: 'New group',
          size: ZeresPluginLibrary.Modals.ModalSizes.SMALL,
          red: false,
          children: [
            ZLibrary.DiscordModules.React.createElement(
              ZLibrary.WebpackModules.getByDisplayName('FormItem'),
              {
                className: ZLibrary.WebpackModules.getByProps('input', 'card').input
              },
              ZLibrary.DiscordModules.React.createElement(ZLibrary.WebpackModules.getByDisplayName('TextInput'), {
                ref: e => {
                  ref = e;
                },
                onChange: value => {
                  ref.props.value = value;
                  ref.forceUpdate();
                },
                autoFocus: 1,
                maxLength: 16
              }),
              ZeresPluginLibrary.DiscordModules.React.createElement(
                ZLibrary.WebpackModules.getByDisplayName('Text'),
                {
                  className: ZLibrary.WebpackModules.getByProps('autocomplete', 'contentTitle').contentTitle,
                  weight: ZLibrary.WebpackModules.getByDisplayName('Text').Weights.SEMIBOLD,
                  size: ZLibrary.WebpackModules.getByDisplayName('Text').Sizes.SMALL
                },
                'User will also be added to the group on save!'
              )
            )
          ],
          onConfirm: () => saveGroup()
        });
      })
    );

    mainItems.push(createSubMenu('Groups', groupSubmenuItems));

    mainItems.push(
      createItem('Set Alias', () => {
        this.saveAliasModal(userId, thisObj.props.user.username);
      })
    );

    ret.props.children.props.children.props.children.push(
      ZeresPluginLibrary.DiscordModules.React.createElement(this.ContextMenuGroup, {
        children: [createSubMenu('Mention Aliases', mainItems)]
      })
    );
  }
}

class EditGroupModal extends BdApi.findModuleByProps('PureComponent').PureComponent {
  constructor(props) {
    super(props);
    this.submitModal = this.submitModal.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }
  componentDidMount() {
    ZLibrary.WebpackModules.getByProps('ComponentDispatch').ComponentDispatch.subscribe(ZLibrary.WebpackModules.getByProps('ComponentActions').ComponentActions.MODAL_SUBMIT, this.submitModal);
    ZLibrary.WebpackModules.getByProps('findDOMNode')
      .findDOMNode(this)
      .focus();
  }
  componentWillUnmount() {
    ZLibrary.WebpackModules.getByProps('ComponentDispatch').ComponentDispatch.unsubscribe(ZLibrary.WebpackModules.getByProps('ComponentActions').ComponentActions.MODAL_SUBMIT, this.submitModal);
  }
  handleSubmit(e) {
    e.preventDefault(), this.submitModal();
  }
  submitModal() {
    ZLibrary.WebpackModules.getByProps('pop').pop();
    null != this.props.onConfirm && this.props.onConfirm(this.props.users, this.props.value);
  }
  handleClose() {
    var e = this.props,
      t = e.onCancel,
      n = e.onClose;
    t && t(), n && n();
    ZLibrary.WebpackModules.getByProps('pop').pop();
  }
  renderAvatar(user) {
    return ZLibrary.DiscordModules.React.createElement(ZLibrary.WebpackModules.getByProps('AnimatedAvatar').AnimatedAvatar, {
      size: 'SIZE_32',
      src: user.getAvatarURL(),
      status: ZLibrary.WebpackModules.getByProps('getStatus').getStatus(user.id),
      isMobile: ZLibrary.WebpackModules.getByProps('isMobileOnline').isMobileOnline(user.id),
      isTyping: false,
      statusTooltip: true
    });
  }
  renderUser(user, noRemove) {
    return ZLibrary.DiscordModules.React.createElement(
      ZLibrary.WebpackModules.getByDisplayName('ListItem'),
      {
        className: ZLibrary.WebpackModules.getByProps('channel').channel,
        avatar: this.renderAvatar(user),
        name: user.username /* possibly their alias? */,
        /* subText: this.renderSubtitle() */ style: {
          maxWidth: 'unset',
          marginLeft: 0
        }
      },
      ZLibrary.DiscordModules.React.createElement(
        ZLibrary.WebpackModules.getByDisplayName('Clickable'),
        {
          className: ZLibrary.WebpackModules.getByProps('closeButton', 'channel').closeButton + ' ' + ZLibrary.WebpackModules.getByProps('clickable').clickable,
          style: {
            display: 'block'
          },
          onClick: () => {
            const idx = this.props.users.findIndex(m => m === user.id);
            if (idx === -1) return;
            this.props.users.splice(idx, 1);
            this.forceUpdate();
          }
        },
        ZLibrary.DiscordModules.React.createElement(ZLibrary.WebpackModules.getByDisplayName('Icon'), {
          className: ZLibrary.WebpackModules.getByProps('closeIcon', 'channel').closeIcon,
          name: 'Nova_Close'
        })
      )
    );
  }
  render() {
    let props = this.props;
    const children = [];
    for (const userId of props.users) {
      const user = ZLibrary.DiscordModules.UserStore.getUser(userId);
      if (!user) continue;
      children.push(this.renderUser(user));
    }
    let ref;
    return ZLibrary.DiscordModules.React.createElement(
      ZLibrary.WebpackModules.getByDisplayName('DeprecatedModal'),
      {
        className: props.className + ' ' + ZLibrary.WebpackModules.getByProps('container', 'mobile').container,
        tag: 'form',
        onSubmit: this.handleSubmit,
        size: ZLibrary.WebpackModules.getByDisplayName('DeprecatedModal').Sizes.SMALL
      },
      ZLibrary.DiscordModules.React.createElement(
        ZLibrary.WebpackModules.getByDisplayName('DeprecatedModal').Header,
        {},
        ZLibrary.DiscordModules.React.createElement(ZLibrary.WebpackModules.getByDisplayName('TextInput'), {
          ref: r => (ref = r),
          value: props.value,
          onChange: function(value) {
            props.value = ref.props.value = value;
            ref.forceUpdate();
          },
          maxLength: 16
        }),
        ZLibrary.DiscordModules.React.createElement(
          ZLibrary.WebpackModules.find(m => m.Link && m.Link.displayName === 'ButtonLink'),
          {
            type: 'button',
            look: ZLibrary.WebpackModules.find(m => m.Link && m.Link.displayName === 'ButtonLink').Looks.LINK,
            color: ZLibrary.WebpackModules.find(m => m.Link && m.Link.displayName === 'ButtonLink').Colors.PRIMARY,
            onClick: this.handleClose
          },
          'Cancel'
        ),
        ZLibrary.DiscordModules.React.createElement(
          ZLibrary.WebpackModules.find(m => m.Link && m.Link.displayName === 'ButtonLink'),
          {
            type: 'submit',
            color: ZLibrary.WebpackModules.find(m => m.Link && m.Link.displayName === 'ButtonLink').Colors.BRAND
          },
          'Save'
        )
      ),
      ZLibrary.DiscordModules.React.createElement(
        ZLibrary.WebpackModules.getByDisplayName('DeprecatedModal').Content,
        {
          className: ZLibrary.WebpackModules.getByProps('container', 'mobile').content
        },
        children
      )
    );
  }
}
