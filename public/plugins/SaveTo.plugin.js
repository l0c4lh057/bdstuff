//META{"name":"SaveTo","source":"https://gitlab.com/_Lighty_/bdstuff/blob/master/public/plugins/SaveTo.plugin.js","website":"https://_lighty_.gitlab.io/bdstuff/?plugin=SaveTo"}*//
/*@cc_on
@if (@_jscript)

	// Offer to self-install for clueless users that try to run this directly.
	var shell = WScript.CreateObject('WScript.Shell');
	var fs = new ActiveXObject('Scripting.FileSystemObject');
	var pathPlugins = shell.ExpandEnvironmentStrings('%APPDATA%\\BetterDiscord\\plugins');
	var pathSelf = WScript.ScriptFullName;
	// Put the user at ease by addressing them in the first person
	shell.Popup('It looks like you\'ve mistakenly tried to run me directly. \n(Don\'t do that!)', 0, 'I\'m a plugin for BetterDiscord', 0x30);
	if (fs.GetParentFolderName(pathSelf) === fs.GetAbsolutePathName(pathPlugins)) {
		shell.Popup('I\'m in the correct folder already.\nJust reload Discord with Ctrl+R.', 0, 'I\'m already installed', 0x40);
	} else if (!fs.FolderExists(pathPlugins)) {
		shell.Popup('I can\'t find the BetterDiscord plugins folder.\nAre you sure it\'s even installed?', 0, 'Can\'t install myself', 0x10);
	} else if (shell.Popup('Should I copy myself to BetterDiscord\'s plugins folder for you?', 0, 'Do you need some help?', 0x34) === 6) {
		fs.CopyFile(pathSelf, fs.BuildPath(pathPlugins, fs.GetFileName(pathSelf)), true);
		// Show the user where to put plugins in the future
		shell.Exec('explorer ' + pathPlugins);
		shell.Popup('I\'m installed!\nJust reload Discord with Ctrl+R.', 0, 'Successfully installed', 0x40);
	}
	WScript.Quit();

@else@*/

class SaveTo {
  getName() {
    return 'SaveTo';
  }
  getVersion() {
    return '1.1.2';
  }
  getAuthor() {
    return 'Lighty';
  }
  getDescription() {
    return 'Allows you to save images, videos, profile icons and server icons to any folder. WIP plugin.';
  }
  load() {}
  start() {
    let onLoaded = () => {
      try {
        if (!global.ZeresPluginLibrary) setTimeout(() => onLoaded(), 1000);
        else this.initialize();
      } catch (err) {
        ZLibrary.Logger.stacktrace(this.getName(), 'Failed to start!', err);
        ZLibrary.Logger.err(this.getName(), `If you cannot solve this yourself, contact ${this.getAuthor()} and provide the errors shown here.`);
        this.stop();
        BdApi.showToast(`[${this.getName()}] Failed to start! Check console (CTRL + SHIFT + I, click console tab) for more error info.`, { type: 'error', timeout: 10000 });
      }
    };
    const getDir = () => {
      // from Zeres Plugin Library, copied here as ZLib may not be available at
      // this point
      const process = require('process');
      const path = require('path');
      if (process.env.injDir) return path.resolve(process.env.injDir, 'plugins/');
      switch (process.platform) {
        case 'win32':
          return path.resolve(process.env.appdata, 'BetterDiscord/plugins/');
        case 'darwin':
          return path.resolve(process.env.HOME, 'Library/Preferences/', 'BetterDiscord/plugins/');
        default:
          return path.resolve(process.env.XDG_CONFIG_HOME ? process.env.XDG_CONFIG_HOME : process.env.HOME + '/.config', 'BetterDiscord/plugins/');
      }
    };
    this.pluginDir = getDir();
    let libraryOutdated = false;
    // I'm sick and tired of people telling me my plugin doesn't work and it's
    // cause zlib is outdated, ffs
    if (!global.ZLibrary || !global.ZeresPluginLibrary || (bdplugins.ZeresPluginLibrary && (libraryOutdated = ZeresPluginLibrary.PluginUpdater.defaultComparator(bdplugins.ZeresPluginLibrary.plugin._config.info.version, '1.2.6')))) {
      const title = libraryOutdated ? 'Library outdated' : 'Library Missing';
      const ModalStack = BdApi.findModuleByProps('push', 'update', 'pop', 'popWithKey');
      const TextElement = BdApi.findModuleByProps('Sizes', 'Weights');
      const ConfirmationModal = BdApi.findModule(m => m.defaultProps && m.key && m.key() == 'confirm-modal');
      const confirmedDownload = () => {
        require('request').get('https://rauenzi.github.io/BDPluginLibrary/release/0PluginLibrary.plugin.js', async (error, response, body) => {
          if (error) return require('electron').shell.openExternal('https://betterdiscord.net/ghdl?url=https://raw.githubusercontent.com/rauenzi/BDPluginLibrary/master/release/0PluginLibrary.plugin.js');
          require('fs').writeFile(require('path').join(this.pluginDir, '0PluginLibrary.plugin.js'), body, () => {
            setTimeout(() => {
              if (!global.bdplugins.ZeresPluginLibrary) return BdApi.alert('Notice', `Due to you using EnhancedDiscord instead of BetterDiscord, you'll have to reload your Discord before ${this.getName()} starts working. Just press CTRL + R to reload and ${this.getName()} will begin to work!`);
              onLoaded();
            }, 1000);
          });
        });
      };
      if (!ModalStack || !ConfirmationModal || !TextElement) {
        BdApi.alert('Uh oh', `Looks like you${libraryOutdated ? 'r Zeres Plugin Library was outdated!' : ' were missing Zeres Plugin Library!'} Also, failed to show a modal, so it has been ${libraryOutdated ? 'updated' : 'downloaded and loaded'} automatically.`);
        confirmedDownload();
        return;
      }
      ModalStack.push(props => {
        return BdApi.React.createElement(
          ConfirmationModal,
          Object.assign(
            {
              header: title,
              children: [
                TextElement({
                  color: TextElement.Colors.PRIMARY,
                  children: [`The library plugin needed for ${this.getName()} is ${libraryOutdated ? 'outdated' : 'missing'}. Please click Download Now to ${libraryOutdated ? 'update' : 'install'} it.`]
                })
              ],
              red: false,
              confirmText: 'Download Now',
              cancelText: 'Cancel',
              onConfirm: () => confirmedDownload()
            },
            props
          )
        );
      });
    } else onLoaded();
  }
  stop() {
    try {
      this.shutdown();
    } catch (err) {
      ZeresPluginLibrary.Logger.stacktrace(this.getName(), 'Failed to stop!', err);
    }
  }
  getChanges() {
    return [
      {
        title: 'added',
        type: 'added',
        items: ['You can now save most file types properly.', 'Saving image, video or audio direct links now works.', 'Changed Save As... in folders to show a modal where you simply input the name.']
      },
      {
        title: 'fixes',
        type: 'fixed',
        items: ['Fixed plugin not working due to context menu changes.', 'Fixed multitude of crashes related to URL parsing, also added failsafe to prevent further crashes.', 'Fixed not being able to save reactions.', 'Fixed incompatibility with MLv2.']
      } /* ,
      {
        title: 'In the works',
        type: 'progress',
        items: ['Will add more options once the library is fixed like sorting and filename type.', 'Will add option to save emotes.']
      } */
      /* ,
      {
        title: 'Improved',
        type: 'improved',
        items: ['Links in edited messages are darkened like normal text to differentiate between edit and current message', 'Edited message color is no longer hardcoded, it is now simply normal text color but darkened, to match theme colors.', 'Toasts no longer show for local user in DMs (you did it, why show it?).', 'Ghost ping toast no longer shows if you are in the channel.']
      } */
    ];
  }
  initialize() {
    ZeresPluginLibrary.PluginUpdater.checkForUpdate(this.getName(), this.getVersion(), `https://_lighty_.gitlab.io/bdstuff/plugins/${this.getName()}.plugin.js`);
    let defaultSettings = {
      fileNameType: 0,
      customFileName: '${file}_${date}_${time}',
      randLength: 7,
      sortMode: 0,
      folders: [],
      displayUpdateNotes: true,
      contextMenuOnBottom: true,
      conflictingFilesHandle: 0,
      versionInfo: ''
    };
    this.settings = ZLibrary.PluginUtilities.loadSettings(this.getName(), defaultSettings);
    if (!this.settings) this.settings = defaultSettings;

    if (typeof this.settings.fileNameType !== 'number' || this.settings.fileNameType > 4) this.settings.fileNameType = 0;
    if (typeof this.settings.sortMode !== 'number') this.settings.sortMode = 0;
    if (typeof this.settings.randLength !== 'number') this.settings.randLength = 0;
    if (!Array.isArray(this.settings.folders)) this.settings.folders = [];
    if (typeof this.settings.customFileName !== 'string') this.settings.customFileName = '${file}_${date}_${time}';

    if (this.settings.versionInfo !== this.getVersion() && this.settings.displayUpdateNotes) {
      ZeresPluginLibrary.Modals.showChangelogModal(this.getName() + ' Changelog', this.getVersion(), this.getChanges());
      this.settings.versionInfo = this.getVersion();
      this.saveSettings();
    }

    const emojiItemSelector = new ZLibrary.DOMTools.Selector(ZLibrary.WebpackModules.getByProps('row', 'emojiPicker').emojiItem).toString();
    const guildIconImageSelector = new ZLibrary.DOMTools.Selector(ZLibrary.WebpackModules.getByProps('guildIconImage').guildIconImage).toString();
    document.body.addEventListener(
      'contextmenu',
      (this.contextmenuEventListener = e => {
        const target = e.target;
        let url;
        let saveType = 'Emote';
        let emoteName = '';
        if (target.matches(emojiItemSelector)) {
          const backgroundImage = target.style.backgroundImage;
          if (!backgroundImage) return;
          url = backgroundImage.split('"')[1];
          emoteName = ZLibrary.Utilities.getNestedProp(ZLibrary.ReactTools.getReactInstance(target), 'key');
          if (!emoteName || !emoteName.length) return;
          else emoteName = emoteName.substr(0, emoteName.lastIndexOf('-', emoteName.lastIndexOf('-') - 1));
        } else if (target.matches(guildIconImageSelector)) {
          const guild = ZLibrary.Utilities.getNestedProp(ZLibrary.ReactTools.getOwnerInstance(target.parentElement), 'props.guild');
          if (!guild) return;
          url = guild.getIconURL(guild.hasFeature('ANIMATED_ICON') ? 'gif' : 'png');
          saveType = 'Icon';
        } else return;
        const menu = new ZeresPluginLibrary.ContextMenu.Menu();
        const createSubMenu = (name, items, callback) => {
          const subMenu = new ZeresPluginLibrary.ContextMenu.Menu();
          subMenu.addItems(...items);
          for (let ch of subMenu.element.children) ch.addClass('clickable-11uBi- da-clickable'); // HACK
          subMenu.element.style.display = 'inline-block';
          subMenu.element.style.position = 'fixed';
          subMenu.element.style.marginLeft = 0;
          const submnu = new ZeresPluginLibrary.ContextMenu.SubMenuItem(name, subMenu);
          if (callback)
            submnu.element.addEventListener('click', () => {
              menu.removeMenu();
              callback();
            });
          submnu.element.style.overflow = 'hidden';
          return submnu;
        };
        const createItem = (name, callback) => {
          const item = new ZeresPluginLibrary.ContextMenu.TextItem(name);
          item.element.addEventListener('click', () => {
            menu.removeMenu();
            if (callback) callback();
          });
          return item;
        };
        const constructedMenu = this.constructMenu(url.split('?')[0], saveType, createSubMenu, createItem, emoteName);
        const subgroup = new ZeresPluginLibrary.ContextMenu.ItemGroup();
        subgroup.addItems(constructedMenu);
        menu.addItems(subgroup);
        menu.element.style.position = 'absolute';
        menu.element.style.zIndex = '99999';
        for (let ch of menu.element.children) ch.addClass('clickable-11uBi- da-clickable'); // HACK
        menu.show(e.clientX, e.clientY);
      })
    );

    this.ContextMenuGroup = ZeresPluginLibrary.DiscordModules.ContextMenuItemsGroup;
    this.ContextMenuItem = ZeresPluginLibrary.DiscordModules.ContextMenuItem;
    this.ContextMenuActions = ZeresPluginLibrary.DiscordModules.ContextMenuActions;
    this.SubMenuItem = ZeresPluginLibrary.WebpackModules.find(m => m.default && m.default.displayName && m.default.displayName.includes('SubMenuItem')).default;

    const GuildContextMenu = ZeresPluginLibrary.WebpackModules.getByDisplayName('GuildContextMenu');
    const NativeLinkGroup = ZeresPluginLibrary.WebpackModules.getByDisplayName('NativeLinkGroup');
    const UserContextMenu = ZeresPluginLibrary.WebpackModules.getByDisplayName('UserContextMenu');

    this.unpatches = [];

    this.unpatches.push(
      ZeresPluginLibrary.Patcher.before(this.getName(), ZLibrary.DiscordModules.ContextMenuActions, 'openContextMenu', (_this, args, ret) => {
        const old = args[1];
        args[1] = e => {
          const ret2 = old(e);
          if (typeof ret2.type !== 'function') return ret2;
          const old2 = ret2.type;
          const onContext = this.handleContextMenu.bind(this);
          ret2.type = function(e) {
            const ret3 = new old2(e);
            if (ret3.render) {
              const old3 = ret3.render.bind(ret3);
              ret3.render = () => {
                const ret4 = old3();
                onContext({ props: e }, null, ret4);
                return ret4;
              };
            } else {
              onContext({ props: e }, null, ret3);
            }
            return ret3;
          };
          return ret2;
        };
      })
    );

    const dialog = require('electron').remote.dialog;
    this.openSaveDialog = dialog.showSaveDialogSync || dialog.showSaveDialog;
    this.openOpenDialog = dialog.showOpenDialogSync || dialog.showOpenDialog;
    this.openItem = require('electron').shell.openItem;
    this.fs = require('fs');
    this.request = require('request');
    this.path = require('path');

    this.createModal.confirmationModal = ZeresPluginLibrary.DiscordModules.ConfirmationModal;
    this.channelMessages = ZeresPluginLibrary.WebpackModules.find(m => m._channelMessages)._channelMessages;

    this.createButton.classes = {
      button: (function() {
        let buttonData = ZeresPluginLibrary.WebpackModules.getByProps('button', 'colorBrand');
        return `${buttonData.button} ${buttonData.lookFilled} ${buttonData.colorBrand} ${buttonData.sizeSmall} ${buttonData.grow}`;
      })(),
      buttonContents: ZeresPluginLibrary.WebpackModules.getByProps('button', 'colorBrand').contents
    };

    this.previewDate = new Date();
    this.previewRand = this.rand();
    this.promises = {
      state: { cancelled: false },
      cancel() {
        this.state.cancelled = true;
      }
    };
    ZeresPluginLibrary.Utilities.suppressErrors(this.patchReactions.bind(this), 'reaction patch')(this.promises.state);
  }
  shutdown() {
    const tryUnpatch = fn => {
      try {
        // things can bug out, best to reload tbh, should maybe warn the user?
        fn();
      } catch (e) {
        ZeresPluginLibrary.Logger.stacktrace(this.getName(), 'Error unpatching', e);
      }
    };
    if (this.unpatches) for (let unpatch of this.unpatches) tryUnpatch(unpatch);
    if (this.contextmenuEventListener) document.body.removeEventListener('contextmenu', this.contextmenuEventListener);
    if (this.promises) this.promises.cancel();
  }
  async patchReactions(promiseState) {
    const Reaction = await ZeresPluginLibrary.ReactComponents.getComponentByName('Reaction', '.reactions-1xb2Ex > div:not(.reactionBtn-2na4rd)');
    if (promiseState.cancelled) return;
    this.unpatches.push(
      ZeresPluginLibrary.Patcher.after(this.getName(), Reaction.component.prototype, 'render', (thisObject, _, returnValue) => {
        const old = returnValue.props.children;
        returnValue.props.children = e => {
          const ret = old(e);
          const props = ret.props;
          const url = thisObject.props.emoji.id
            ? ZeresPluginLibrary.WebpackModules.getByProps('getEmojiURL').getEmojiURL({
                id: thisObject.props.emoji.id,
                animated: thisObject.props.emoji.animated
              })
            : ZeresPluginLibrary.WebpackModules.getByProps('getURL').getURL(thisObject.props.emoji.name);
          props.onContextMenu = e => {
            const createSubmenu = (name, items, callback) => {
              return ZeresPluginLibrary.DiscordModules.React.createElement(this.SubMenuItem, {
                label: name,
                render: items,
                invertChildY: false,
                action: () => {
                  if (callback) {
                    this.ContextMenuActions.closeContextMenu();
                    callback();
                  }
                }
              });
            };
            const createItem = (name, callback) => {
              return ZeresPluginLibrary.DiscordModules.React.createElement(this.ContextMenuItem, {
                label: name,
                action: () => {
                  this.ContextMenuActions.closeContextMenu();
                  if (callback) callback();
                }
              });
            };
            const submenu = this.constructMenu(url.split('?')[0], 'Reaction', createSubmenu, createItem, thisObject.props.emoji.name);
            ZLibrary.DiscordModules.ContextMenuActions.openContextMenu(e, e => {
              return ZLibrary.DiscordModules.React.createElement(
                'div',
                {
                  className: ZLibrary.DiscordClasses.ContextMenu.contextMenu
                },
                ZLibrary.DiscordModules.React.createElement(this.ContextMenuGroup, {
                  children: [submenu]
                })
              );
            });
          };
          return ret;
        };
      })
    );
    Reaction.forceUpdateAll();
  }
  loadData(name, construct = {}) {
    try {
      return Object.assign(construct, BdApi.getData(name, 'settings')); // zeres library turns all arrays into
      // objects.. for.. some reason?
    } catch (e) {
      return {}; // error handling not done here
    }
  }
  saveSettings() {
    ZeresPluginLibrary.PluginUtilities.saveSettings(this.getName(), this.settings);
  }
  buildSetting(data) {
    // compied from ZLib actually
    const { name, note, type, value, onChange, id } = data;
    let setting = null;
    if (type == 'color') {
      setting = new ZeresPluginLibrary.Settings.ColorPicker(name, note, value, onChange, { disabled: data.disabled }); // DOESN'T WORK, REEEEEEEEEE
    } else if (type == 'dropdown') {
      setting = new ZeresPluginLibrary.Settings.Dropdown(name, note, value, data.options, onChange);
    } else if (type == 'file') {
      setting = new ZeresPluginLibrary.Settings.FilePicker(name, note, onChange);
    } else if (type == 'keybind') {
      setting = new ZeresPluginLibrary.Settings.Keybind(name, note, value, onChange);
    } else if (type == 'radio') {
      setting = new ZeresPluginLibrary.Settings.RadioGroup(name, note, value, data.options, onChange, { disabled: data.disabled });
    } else if (type == 'slider') {
      const options = {};
      if (typeof data.markers != 'undefined') options.markers = data.markers;
      if (typeof data.stickToMarkers != 'undefined') options.stickToMarkers = data.stickToMarkers;
      setting = new ZeresPluginLibrary.Settings.Slider(name, note, data.min, data.max, value, onChange, options);
    } else if (type == 'switch') {
      setting = new ZeresPluginLibrary.Settings.Switch(name, note, value, onChange, { disabled: data.disabled });
    } else if (type == 'textbox') {
      setting = new ZeresPluginLibrary.Settings.Textbox(name, note, value, onChange, { placeholder: data.placeholder || '' });
    }
    if (id) setting.getElement().id = id;
    return setting;
  }
  createSetting(data) {
    if (data instanceof Element) return data;
    const current = Object.assign({}, data);
    if (!current.onChange) {
      current.onChange = value => {
        this.settings[current.id] = value;
        if (current.callback) current.callback(value);
      };
    }
    if (typeof current.value === 'undefined') current.value = this.settings[current.id];
    return this.buildSetting(current);
  }
  createGroup(group) {
    const { name, id, collapsible, shown, settings } = group;

    const list = [];
    for (let s = 0; s < settings.length; s++) list.push(this.createSetting(settings[s]));

    const settingGroup = new ZeresPluginLibrary.Settings.SettingGroup(name, { shown, collapsible }).append(...list);
    settingGroup.group.id = id; // should generate the id in here instead?
    return settingGroup;
  }
  getSettingsPanel() {
    // todo, sort out the menu
    const list = [];
    const updatePreview = () => {
      const preview = document.getElementById('st-preview');
      preview.innerText = this.formatFilename('unknown', true) + '.png';
    };
    list.push(
      this.createGroup({
        name: 'File save settings',
        collapsible: true,
        shown: false,
        settings: [
          ZLibrary.DOMTools.parseHTML(`<div class="${ZLibrary.WebpackModules.getByProps('marginBottom20').marginBottom20}">
          <h5 class="h5-18_1nd title-3sZWYQ da-h5 da-title defaultMarginh5-2mL-bP da-defaultMarginh5">
            File name save preview
          </h5>
          <div class="${ZeresPluginLibrary.WebpackModules.getByProps('defaultColor').defaultColor}" id="st-preview">
          ${this.formatFilename('unknown', true)}.png
          </div>
          </div>`),
          {
            name: 'File name save',
            id: 'fileNameType',
            type: 'dropdown',
            options: [
              {
                label: 'Original',
                value: 0
              },
              {
                label: 'Date',
                value: 1
              },
              {
                label: 'Random',
                value: 2
              },
              {
                label: 'Original + random',
                value: 3
              },
              {
                label: 'Custom',
                value: 4
              }
            ],
            callback: updatePreview
          },
          {
            name: 'Custom file name save',
            note: 'Available options: file rand date time day month year hours minutes seconds. options must be wrapped in ${<OPTION>}!',
            id: 'customFileName',
            type: 'textbox',
            callback: updatePreview
          },
          {
            name: 'Random string length',
            note: '11 chars max!',
            id: 'randLength',
            type: 'textbox',
            onChange: val => {
              if (!val) return;
              if (isNaN(val)) return ZLibrary.Toasts.show('Value must be a number!', { type: 'error' });
              this.settings.randLength = parseInt(val);
              this.previewRand = this.rand();
              updatePreview();
            }
          },
          {
            name: 'Conflicting filename mode',
            id: 'conflictingFilesHandle',
            type: 'radio',
            options: [
              {
                name: 'Warn',
                value: 0
              },
              {
                name: 'Overwrite',
                value: 1
              },
              {
                name: 'Append number: (1)',
                value: 2
              },
              {
                name: 'Append random',
                value: 3
              }
            ]
          }
        ]
      })
    );
    list.push(
      this.createGroup({
        name: 'Misc',
        collapsible: true,
        shown: false,
        settings: [
          {
            name: 'Context menu option at the bottom instead of top',
            id: 'contextMenuOnBottom',
            type: 'switch'
          },
          {
            name: 'Show update notes on update',
            id: 'displayUpdateNotes',
            type: 'switch'
          }
        ]
      })
    );

    const div = document.createElement('div');
    div.id = 'st-settings-buttonbox';
    div.style.display = 'inline-flex';
    div.appendChild(this.createButton('Changelog', () => ZeresPluginLibrary.Modals.showChangelogModal(this.getName() + ' Changelog', this.getVersion(), this.getChanges())));
    div.appendChild(this.createButton('Donate', () => this.nodeModules.electron.shell.openExternal('https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DZUL9UZDDRLB8&source=url')));
    let button = div.firstElementChild;
    while (button) {
      button.style.marginRight = button.style.marginLeft = `5px`;
      button = button.nextElementSibling;
    }

    list.push(div);

    return ZeresPluginLibrary.Settings.SettingPanel.build(_ => this.saveSettings(), ...list);
  }
  createButton(label, callback) {
    const classes = this.createButton.classes;
    const ret = ZLibrary.DOMTools.parseHTML(`<button type="button" class="${classes.button}"><div class="${classes.buttonContents}">${label}</div></button>`);
    if (callback) ret.addEventListener('click', callback);
    return ret;
  }
  createModal(options, name) {
    const modal = this.createModal.confirmationModal;
    ZeresPluginLibrary.DiscordModules.ModalStack.push(function(props) {
      return ZeresPluginLibrary.DiscordModules.React.createElement(modal, Object.assign(options, props));
    });
  }
  constructMenu(url, type, createSubMenu, createItem, customName) {
    const subItems = [];
    const folderSubMenus = [];
    const formattedurl = this.formatURL(url, type === 'Icon' || type === 'Avatar', customName);

    const download = (path, openOnSave) => {
      const req = this.request(formattedurl.url);
      req.on('response', res => {
        if (res.statusCode == 200) {
          req
            .pipe(this.fs.createWriteStream(path))
            .on('finish', () => {
              if (openOnSave) this.openItem(path);
              BdApi.showToast('Saved!', { type: 'success' });
            })
            .on('error', e => BdApi.showToast(`Failed to save! ${e}`, { type: 'error', timeout: 10000 }));
        } else if (res.statusCode == 404) BdApi.showToast('Image does not exist!', { type: 'error' });
        else BdApi.showToast('Unknown error.', { type: 'error' });
      });
    };

    const saveFile = (path, basePath, openOnSave, dontWarn) => {
      if (!dontWarn && this.fs.existsSync(path)) {
        const handleConflict = mode => {
          switch (mode) {
            case 2: {
              let num = 1;
              try {
                while (this.fs.existsSync(path)) {
                  path = `${basePath}/${formattedurl.name}(${num}).${formattedurl.extension}`;
                  num++;
                  if (num > 99) return ZeresPluginLibrary.Logger.err(this.getName(), 'Save attempt passed num 99!');
                }
              } catch (e) {
                return ZeresPluginLibrary.Logger.stacktrace(this.getName(), 'Error finding good number', e);
              }
              break;
            }
            case 3: {
              path = `${basePath}/${formattedurl.name}-${this.rand()}.${formattedurl.extension}`;
            }
          }
          download(path, openOnSave);
        };
        if (this.settings.conflictingFilesHandle) {
          handleConflict(this.settings.conflictingFilesHandle);
        } else {
          let ref1, ref2;
          this.createModal({
            confirmText: 'Ok',
            cancelText: 'Cancel',
            header: 'File conflict',
            size: ZeresPluginLibrary.Modals.ModalSizes.SMALL,
            red: false,
            children: [
              ZeresPluginLibrary.DiscordModules.React.createElement(
                ZeresPluginLibrary.WebpackModules.getByDisplayName('Text'),
                {
                  className: ZeresPluginLibrary.WebpackModules.getByProps('defaultColor').defaultColor + ' ' + ZLibrary.WebpackModules.getByProps('marginBottom20').marginBottom20
                },
                'A file with the existing name already exists! What do you want to do?'
              ),
              ZeresPluginLibrary.DiscordModules.React.createElement(ZLibrary.DiscordModules.RadioGroup, {
                className: ZLibrary.WebpackModules.getByProps('marginBottom20').marginBottom20,
                clearable: false,
                searchable: false,
                options: [
                  {
                    name: 'Overwrite',
                    value: 1
                  },
                  {
                    name: 'Append number: (1)',
                    value: 2
                  },
                  {
                    name: 'Append random',
                    value: 3
                  }
                ],
                value: 1,
                ref: e => (ref1 = e),
                onChange: e => {
                  ref1.props.value = e.value;
                  ref1.forceUpdate();
                }
              }),
              ZeresPluginLibrary.DiscordModules.React.createElement(ZLibrary.DiscordModules.SwitchRow, {
                children: 'Disable this warning and save this option',
                note: 'This can be changed in settings',
                value: 0,
                ref: e => (ref2 = e),
                onChange: e => {
                  const checked = e.currentTarget.checked;
                  ref2.props.value = checked;
                  ref2.forceUpdate();
                }
              })
            ],
            onConfirm: () => {
              if (ref2.props.value) this.settings.conflictingFilesHandle = ref1.props.value;
              handleConflict(ref1.props.value);
            }
          });
        }
      } else {
        download(path, openOnSave);
      }
    };

    const folderSubMenu = folder => {
      return createSubMenu(
        folder.name,
        [
          createItem('Remove Folder', () => {
            const index = this.settings.folders.findIndex(m => m === folder);
            if (index === -1) return BdApi.showToast("Fatal error! Attempted to remove a folder that doesn't exist!", { type: 'error', timeout: 5000 });
            this.settings.folders.splice(index, 1);
            this.saveSettings();
            BdApi.showToast('Removed!', { type: 'success' });
          }),
          createItem('Open Folder', () => {
            this.openItem(folder.path);
          }),
          createItem('Save', () => {
            const path = folder.path + `/${formattedurl.fileName}`;
            saveFile(path, folder.path);
          }),
          createItem('Save As...', () => {
            let val = '';
            let inputRef = null;
            ZLibrary.Modals.showModal(
              'Save as...',
              ZLibrary.DiscordModules.React.createElement(
                ZLibrary.WebpackModules.getByDisplayName('FormItem'),
                {
                  className: ZLibrary.DiscordClasses.Margins.marginBottom20.value,
                  title: 'Name your file'
                },
                ZLibrary.DiscordModules.React.createElement(ZLibrary.WebpackModules.getByDisplayName('TextInput'), {
                  maxLength: ZLibrary.DiscordModules.DiscordConstants.MAX_GUILD_FOLDER_NAME_LENGTH,
                  ref: e => (inputRef = e),
                  value: val,
                  onChange: e => {
                    val = e;
                    inputRef.props.value = e;
                    inputRef.forceUpdate();
                  },
                  placeholder: formattedurl.name,
                  autoFocus: true
                })
              ),
              {
                confirmText: 'Save',
                onConfirm: () => {
                  if (!val.length) val = formattedurl.name;
                  else formattedurl.name = val;
                  saveFile(folder.path + `/${val}.${formattedurl.extension}`, folder.path, false, false);
                }
              }
            );
            /* const path = this.openSaveDialog({ defaultPath: folder.path + `/${formattedurl.fileName}` });
            if (!path) return BdApi.showToast('Maybe next time.');
            saveFile(path, undefined, false, true); */
          }),
          createItem('Save And Open', () => {
            const path = folder.path + `/${formattedurl.fileName}`;
            saveFile(path, folder.path, true);
          }),
          createItem('Edit', () => {
            let ref1;
            let ref2;
            const saveFolder = () => {
              folder.name = ref1.props.value;
              folder.path = ref2.props.value;
              this.saveSettings();
            };
            const selectFolder = () => {
              const path = this.openOpenDialog({
                title: 'Select folder',
                properties: ['openDirectory', 'createDirectory']
              });
              if (!path) return BdApi.showToast('Maybe next time.');
              let idx;
              if ((idx = this.settings.folders.findIndex(m => m.path === path[0])) !== -1) return BdApi.showToast(`Folder already exists as ${this.settings.folders[idx].name}!`, { type: 'error', timeout: 5000 });
              ref2.props.value = path[0];
              ref2.forceUpdate();
            };
            this.createModal({
              confirmText: 'Save',
              cancelText: 'Cancel',
              header: 'Edit folder',
              size: ZeresPluginLibrary.Modals.ModalSizes.SMALL,
              red: false,
              children: [
                ZLibrary.DiscordModules.React.createElement(
                  ZLibrary.WebpackModules.getByDisplayName('FormItem'),
                  {
                    title: 'Folder name',
                    className: ZLibrary.WebpackModules.getByProps('input', 'card').input
                  },
                  ZLibrary.DiscordModules.React.createElement(ZLibrary.WebpackModules.getByDisplayName('TextInput'), {
                    ref: e => {
                      ref1 = e;
                    },
                    value: folder.name,
                    onChange: value => {
                      ref1.props.value = value;
                      ref1.forceUpdate();
                    },
                    autoFocus: 1
                  })
                ),
                ZLibrary.DiscordModules.React.createElement(
                  ZLibrary.WebpackModules.getByDisplayName('FormItem'),
                  {
                    title: 'Folder path',
                    className: ZLibrary.WebpackModules.getByProps('input', 'card').input
                  },
                  ZLibrary.DiscordModules.React.createElement(ZLibrary.WebpackModules.getByDisplayName('TextInput'), {
                    ref: e => {
                      ref2 = e;
                    },
                    value: folder.path,
                    onChange: value => {
                      ref2.props.value = value;
                      ref2.forceUpdate();
                    }
                  })
                ),
                ZLibrary.DiscordModules.React.createElement(
                  ZLibrary.WebpackModules.find(m => m.Link && m.Link.displayName === 'ButtonLink'),
                  {
                    color: ZLibrary.WebpackModules.find(m => m.Link && m.Link.displayName === 'ButtonLink').Colors.BRAND,
                    style: { width: '100%' },
                    onClick: () => selectFolder()
                  },
                  'Browse'
                )
              ],
              onConfirm: () => saveFolder()
            });
          })
        ],
        () => {
          const path = folder.path + `/${formattedurl.fileName}`;
          saveFile(path, folder.path);
        }
      );
    };
    for (const folder of this.settings.folders) folderSubMenus.push(folderSubMenu(folder));
    subItems.push(
      ...folderSubMenus,
      createItem('Add Folder', () => {
        const path = this.openOpenDialog({
          title: 'Add folder',
          properties: ['openDirectory', 'createDirectory']
        });
        if (!path) return BdApi.showToast('Maybe next time.');
        let idx;
        if ((idx = this.settings.folders.findIndex(m => m.path === path[0])) !== -1) return BdApi.showToast(`Folder already exists as ${this.settings.folders[idx].name}!`, { type: 'error', timeout: 5000 });
        const folderName = this.path.basename(path[0]);
        let ref;
        const saveFolder = () => {
          this.settings.folders.push({
            path: path[0],
            name: ref.props.value,
            position: this.settings.folders.length
          });
          this.saveSettings();
          BdApi.showToast('Added!', { type: 'success' });
        };
        this.createModal({
          confirmText: 'Save',
          cancelText: 'Cancel',
          header: 'New folder',
          size: ZeresPluginLibrary.Modals.ModalSizes.SMALL,
          red: false,
          children: [
            ZLibrary.DiscordModules.React.createElement(
              ZLibrary.WebpackModules.getByDisplayName('FormItem'),
              {
                className: ZLibrary.WebpackModules.getByProps('input', 'card').input
              },
              ZLibrary.DiscordModules.React.createElement(ZLibrary.WebpackModules.getByDisplayName('TextInput'), {
                ref: e => {
                  ref = e;
                },
                value: folderName,
                onChange: value => {
                  ref.props.value = value;
                  ref.forceUpdate();
                },
                autoFocus: 1
              })
            )
          ],
          onConfirm: () => saveFolder()
        });
      }),
      createItem('Save As...', () => {
        const path = this.openSaveDialog({ defaultPath: formattedurl.fileName });
        if (!path) return BdApi.showToast('Maybe next time.');
        saveFile(path, undefined, false, true);
      })
    );
    return createSubMenu(`Save ${type} To`, subItems);
  }
  rand() {
    return Math.random()
      .toString(36)
      .substr(2, this.settings.randLength);
  }
  formatFilename(name, preview) {
    const date = (preview && this.previewDate) || new Date();
    const rand = (preview && this.previewRand) || this.rand();
    switch (this.settings.fileNameType) {
      case 0: // original
        return name;
      case 1: // date
        return `${date
          .toLocaleDateString()
          .split('/')
          .join('-')} ${date.getHours()}-${date.getMinutes()}-${date.getSeconds()}`;
      case 2: // random
        return rand;
      case 3: // original + random
        return `${name}-${rand}`;
      case 4: // custom
        // options file rand date time day month year hours minutes seconds
        return ZLibrary.Utilities.formatTString(this.settings.customFileName, {
          rand,
          file: name,
          date: date
            .toLocaleDateString()
            .split('/')
            .join('-'),
          time: `${date.getMinutes()}-${date.getSeconds()}-${date.getMilliseconds()}`,
          day: date.getDay(),
          month: date.getMonth(),
          year: date.getFullYear(),
          hours: date.getHours(),
          minutes: date.getMinutes(),
          seconds: date.getSeconds()
        });
    }
    return 'INTERNAL_ERROR';
  }
  formatURL(url, requiresSize, customName) {
    if (url.indexOf('/a_') !== -1) url = url.replace('.webp', '.gif').replace('.png', '.gif');
    else url = url.replace('.webp', '.png');
    let fileName = url.substr(url.lastIndexOf('/') + 1);
    if (requiresSize) url += '?size=2048';
    const match = fileName.match(/(.*)\.([0-9a-zA-Z]+)([^\/]+)?$/);
    let name = customName || match[1];
    let extension = match[2];
    if (!extension) {
      extension = name;
      name = 'unknown';
    }
    name = this.formatFilename(name);
    return { fileName: `${name}.${extension}`, url: url, name, extension };
  }
  handleContextMenu(thisObj, args, returnValue) {
    if (!returnValue) return returnValue;

    const type = thisObj.props.type;

    const createSubmenu = (name, items, callback) => {
      return ZeresPluginLibrary.DiscordModules.React.createElement(this.SubMenuItem, {
        label: name,
        render: items,
        invertChildY: false,
        action: () => {
          if (callback) {
            this.ContextMenuActions.closeContextMenu();
            callback();
          }
        }
      });
    };

    const createItem = (name, callback) => {
      return ZeresPluginLibrary.DiscordModules.React.createElement(this.ContextMenuItem, {
        label: name,
        action: () => {
          this.ContextMenuActions.closeContextMenu();
          if (callback) callback();
        }
      });
    };

    let saveType = 'File';
    let url = '';
    let customName = '';
    // image has no type property
    if (type === 'NATIVE_IMAGE' || type === 'MESSAGE_MAIN') {
      let src = (type === 'NATIVE_IMAGE' && ((typeof ZLibrary.Utilities.getNestedProp(returnValue, 'props.children.props.href') === 'string' && returnValue.props.children.props.href.indexOf('discordapp.com/channels') === -1 && returnValue.props.children.props.href) || ZLibrary.Utilities.getNestedProp(returnValue, 'props.children.props.src'))) || ZLibrary.Utilities.getNestedProp(thisObj, 'props.attachment.url');
      if (!src) {
        let C = thisObj.props.target;
        let t;
        let e;
        while (null != C) {
          C instanceof HTMLImageElement && null != C.src && (t = C.src), C instanceof HTMLAnchorElement && null != C.href && (e = C.href), (C = C.parentNode);
        }
        if (!t && !(/\.(png|jpe?g|webp|gif)$/i.test(e) || /\.(mp4|webm|mov)$/i.test(e) || /\.(mp3|ogg|wav|flac)$/i.test(e))) return;
        src = t || e;
        if (!src) return;
      }
      url = src;
      if (!url) return;
      if (/\.(png|jpe?g|webp|gif)$/i.test(url)) saveType = 'Image';
      if (/\.(mp4|webm|mov)$/i.test(url)) saveType = 'Video';
      if (/\.(mp3|ogg|wav|flac)$/i.test(url)) saveType = 'Audio';
      else if (url.indexOf('app.com/emojis/') !== -1) {
        saveType = 'Emoji';
        const emojiId = url.split('emojis/')[1].split('.')[0];
        const emoji = ZLibrary.DiscordModules.EmojiUtils.getDisambiguatedEmojiContext().getById(emojiId);
        if (!emoji) {
          if (!ZLibrary.DiscordAPI.currentChannel || !this.channelMessages[ZLibrary.DiscordAPI.currentChannel.id]) return;
          const message = this.channelMessages[ZLibrary.DiscordAPI.currentChannel.id]._array.find(m => m.content.indexOf(emojiId) !== -1);
          if (!message || !message.content) return;
          const group = message.content.match(new RegExp(`<a?:([^:>]*):${emojiId}>`));
          if (!group || !group[1]) return;
          customName = group[1];
        } else customName = emoji.name;
      }
      if (!Array.isArray(returnValue.props.children)) returnValue.props.children = [returnValue.props.children];
    } else if (type === 'GUILD_ICON_BAR') {
      saveType = 'Icon';
      url = thisObj.props.guild.getIconURL();
      if (!url) return;
      /* customName = thisObj.props.guild.name; */
    } else {
      saveType = 'Avatar';
      if (!thisObj.props.user || !thisObj.props.user.getAvatarURL) return /* ZLibrary.Logger.warn(this.getName(), `Something went wrong, user or avatar URL === undefined, unknown context menu type "${type}" ?`); */;
      url = thisObj.props.user.getAvatarURL();
      if (url.startsWith('/assets/')) url = 'https://discordapp.com' + url;
      /* customName = thisObj.props.user.username; */
    }
    try {
      const submenu = this.constructMenu(url.split('?')[0], saveType, createSubmenu, createItem, customName);
      const group = ZeresPluginLibrary.DiscordModules.React.createElement(this.ContextMenuGroup, { children: [submenu] });
      let targetGroup;
      if (type === 'NATIVE_IMAGE' || type === 'MESSAGE_MAIN' || type === 'GUILD_ICON_BAR') targetGroup = returnValue.props.children;
      else targetGroup = returnValue.props.children.props.children.props.children;

      if (this.settings.contextMenuOnBottom) targetGroup.push(group);
      else targetGroup.unshift(group);
    } catch (e) {
      ZLibrary.Logger.warn(this.getName(), 'Failed to parse image...', url);
    }
  }
}

/*@end@*/
